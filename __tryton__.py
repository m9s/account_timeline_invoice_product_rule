# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Placeholder for module account_timeline_invoice_product_rule',
    'name_de_DE': 'Platzhalter für '
        'Modul account_timeline_invoice_product_rule',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Module placeholder

        * Functionality moved to module `account_product_rule`
    ''',
    'description_de_DE': '''Platzhaltermodul

        * Die Funktionalität dieses Moduls wurde nach `account_product_rule`
          verschoben.
    ''',
    'depends': [
        'ir',
        'res',
    ],
    'xml': [
    ],
    'translation': [
    ],
}
